[[_TOC_]]

# Resources
## <img src="img/unityLogo.PNG" title="Unity" height="64">

### [2D Movement](./Unity/2DMovement.cs)
(Based on [Mix and Jam](https://www.youtube.com/watch?v=STyY26a_dPY))

<img src="img/2dmov.gif">

#### Instructions
1. Add the *Player Movement* component to the object.
2. Set the *LayerMask* for ground detection.
3. Set the properties as wanted. The recommended values are in the script.
4. (KeyBinding in the future).

### 3rd Person Controller: 
To add

### [2D Flocking Simulation](./Unity/Boid.cs)
<img src="img/flock.gif">

#### Instructions
- Add the Boid component to the *gameObject*.
- Add a *Collider* and set the layer for the *boid*.

#### Parameters
- **obstacleMask:** layer of objects that boid will avoid.
- **boidMask:** layer of objects that boid will interact with.
- **isLeader:** whether or not the surrounding boids should follow this boid.
- **distanceToLeader:** target separation when following leader.
- **vision:** perception radius.
- **speed:** max speed of the boid.
- **maxForce:** maximum amount of force that will be applied to a boid each frame.
- **alignWeight, cohesionWeight, separationWeight:** they control the behaviour of the flock.
- **targetSeparation:** target distance between boids.
- **damping:** how much a boid is allowed to turn each frame.

### [2D Light Bounce](./Unity/LightBounce.cs):
<img src="img/light.gif">

### [Popup Text](./Unity/PopupText.unitypackage):
<img src="img/PopupGif.gif">

#### Example usage
~~~c#
PopupText.Show("Test", Vector3.zero, Color.cyan);
~~~


### 2D Rewind
To add.

#### Instructions
1. Create child gameObject for the spawn position.
2. Add a *LineRenderer* component to the main gameObject for the light beam (the code can be tweeked to work with 2D lights). 
3. Add the *Light Emitter* component to the main gameObject and set the properties:
    - **Max Reflections:** max number of times that light can bounce.
    - **Max Distance:** max length of the beam.
    - **Bounce Layer:** determines in which objects the light can bounce.
    - **Spawn Point:** *Transform* created in (1).
    - **Line:** *LineRenderer* created in (2).

### Cube Explosion
To add

### Ghost Player
To add


### [Mobile Gyroscope Aiming](./Unity/GyroscopeAiming.cs):
<img src="img/aim.gif">

### [Pivot 3D Camera](./Unity/PivotCamera.cs):
#### Instructions
Parameters:
- **target:** *Transform* to which the camera will pivot around.
- **speed:** movement velocity.
- **minDistance:** the farthest distance of the camera to the *target*.
- **maxDistance:** the closest distance of the camera to the *target*.

Movement:
- By default the *Vertical* and *Horizontal* axis rotates around the target. When *left click* is pressed, *Vertical* axis controls the distance tho the target.

#### Instructions
Just drag the component to the object. You can adjust the speed on each axis on the inspector.

### [2D Strecth](./Unity/StrecthVelocity.cs)
(Based on code from Internet, not source available).
<img src="img/stretch.gif">

#### Instructions
1. Add an empty *gameObject* to the desired object (with a *Rigidbody* attached) and attach *Squash Velocity* component in it.
2. Add the Sprite Renderer as a child to the *gameObject* of (1).
3. Set the properties:
    - **Sprite Object:** *Transform* of the child *Sprite Renderer*.
    - **Stretch:** force of the stretching effect.
    - **rb:** reference to the rigidbody of (1).


## <img src="img/reactLogo.svg" width="64">

### [Matrix Visualiation Tool (Material UI)](./ReactJS/Matrix.js): 

  <img src="img/matrix.gif">

  #### Instructions
  - **Arrow Keys:** change selected cell of matrix.
  - **Right Click on Buttons:** add row/collumn.
  - **Left Click on Buttons:** remove row/collumn.

  #### Example

  ~~~javascript
  import Matrix from "./Matrix";
  
  export default class Container extends Component {
      constructor(props) {
      super(props);
      this.state = {
        matrices: [
          [
            [1, 2],
            [3, 4]
          ]
        ]
      };
      this.updateMatrix = this.updateMatrix.bind(this);
    }
      
    updateMatrix = (index, data) => {
      let matrices = [...this.state.matrices];
      let matrix = { ...matrices[index] };
  
      matrix = data;
      matrices[index] = matrix;
  
      this.setState({ matrices });
    };
  
    callbackFunction = (childData, index) => {
      this.updateMatrix(index, childData);
    };
  
    matrix = (index, data) => {
      return (
        <Matrix
          rows={this.state.matrices[index]}
          index={index}
          functionCallFromParent={this.callbackFunction.bind(this)}
        />
      );
    };
  
    render(){
        return({this.matrix(0)})
    }
  }
  ~~~

  
