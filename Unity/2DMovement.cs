using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class 2DMovement : MonoBehaviour
{
    Rigidbody2D rb;

    [Header("WALK")]
    [SerializeField] float speed = 10;
    private bool canMove = true;

    [Header("Collisions")]
    [SerializeField] LayerMask groundLayer;
    private bool grounded;
    private bool onWall;
    private bool onRightWall;
    private bool onLeftWall;
    [SerializeField] float colRadius = .25f;
    [SerializeField] Vector2 feetOffset;
    [SerializeField] Vector2 rightOffset;
    [SerializeField] Vector2 leftOffset;

    [Header("JUMP")]
    [SerializeField] float jumpForce = 50;
    [SerializeField] float wallJumpLerp = 10;
    [SerializeField] float fallMultiplier = 5f;
    [SerializeField] float lowJumpMultiplier = 5f;
    private int jumpCount = 0;
    [SerializeField] int extraJumps = 2;
    private bool wallJumped = false;

    [Header("DASH")]
    private bool isDashing = false;
    private bool canDash = true;
    private float initialGravityScale;
    [SerializeField] float dashRecharge = 0.5f;
    [SerializeField] float dashSpeed = 30f;
    [SerializeField] float dashDuration = 0.15f;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        initialGravityScale = rb.gravityScale;
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float xRaw = Input.GetAxisRaw("Horizontal");
        float yRaw = Input.GetAxisRaw("Vertical");

        #region Walk
        Vector2 walkDir = new Vector2(x, y);
        Walk(walkDir);
        #endregion

        #region Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(grounded || jumpCount<extraJumps)
                Jump(Vector2.up);
            else if (onWall)
                WallJump();                
        }

        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
        #endregion

        #region Dash
        if (Input.GetKeyDown(KeyCode.E) && !isDashing)
        {
            Vector2 dashDir = new Vector2(x, y);
            if (dashDir.magnitude == 0)
                dashDir = new Vector2(1, 0);

            Dash(dashDir);
        }
        #endregion
        if (grounded)
        {
            jumpCount = 0;
            wallJumped = false;
        }

    }

    private void Walk(Vector2 dir)
    {
        if (!canMove)
            return;

        Vector2 velocity = Vector2.zero;

        if (!wallJumped)
            velocity = new Vector2(dir.x * speed, rb.velocity.y);
        else
        {
            Vector2 direction = new Vector2(dir.x * speed, rb.velocity.y);
            velocity = Vector2.Lerp(rb.velocity, direction, wallJumpLerp * Time.deltaTime);
        }

        rb.velocity = velocity;
    }

    private void Jump(Vector2 dir)
    {
        jumpCount++;
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * jumpForce;
    }

    private void WallJump()
    {
        wallJumped = true;
        StopCoroutine(DisableMovement(0));
        StartCoroutine(DisableMovement(.1f));

        rb.velocity = Vector2.zero;

        Vector2 wallDir = onRightWall ? Vector2.left : Vector2.right;
        Vector2 dir = (Vector2.up / 1.5f) + (wallDir / 1.5f);
        Jump(dir);
    }

    void Dash(Vector2 dir)
    {
        isDashing = true;

        rb.velocity = Vector2.zero;
        rb.gravityScale = 0;

        rb.velocity += dir.normalized * dashSpeed;

        StartCoroutine(DashWait());
    }

    IEnumerator DashWait()
    {
        //trailRenderer.emitting = true;
        canMove = false;
        yield return new WaitForSeconds(dashDuration);
        //trailRenderer.emitting = false;

        isDashing = false;
        canDash = false;
        canMove = true;

        rb.velocity = Vector2.zero;
        rb.gravityScale = initialGravityScale;

        yield return new WaitForSeconds(dashRecharge);
        canDash = true;
    }
    IEnumerator DisableMovement(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;
    }
}
