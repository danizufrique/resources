using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Boid : MonoBehaviour
{
    private Boid[] nearbyBoids;
    private int nBoids;

    public LayerMask obstacleMask;
    public LayerMask boidMask;

    public bool isLeader = false;
    public float distanceToLeader = 1.5f;
    public float vision = 2;
    public float speed = 10;
    public float maxForce = 0.1f;
    public float alignWeight = 1.0f;
    public float cohesionWeight = 1.0f;
    public float separationWeight = 1.5f;
    public float targetSeparation = 1.5f;
    public float damping = 40f;
    private float screenHeight, screenWidth;

    private Vector2 position;
    private Vector2 velocity;
    private Vector2 acceleration;

    private void Start()
    {
        screenHeight = Camera.main.orthographicSize * 2;
        screenWidth = Camera.main.aspect * screenHeight;

        position = transform.position;
        velocity = new Vector2(UnityEngine.Random.Range(-1, 1), UnityEngine.Random.Range(-1, 1)) * speed;
        acceleration = Vector2.zero;
        transform.right = velocity;
    }

    private Vector2 ComputeAcceleration()
    {
        Vector2 align = Vector2.zero;
        Vector2 cohesion = Vector2.zero;
        Vector2 separation = Vector2.zero;

        foreach (Boid b in nearbyBoids)
        {
            float d = Vector2.Distance(position, b.position);

            align += b.velocity;
            cohesion += b.position;
            
            if (d > 0 && d < targetSeparation)
            {
                Vector2 dif = position - b.position;
                dif = dif.normalized / d;
                separation += dif;
            }
        }

        if (nBoids > 0)
        {
            align /= (float)nBoids;
            align = align.normalized * speed;
            align -= velocity;
            align = Vector2.ClampMagnitude(align, maxForce);

            cohesion /= nBoids;
            cohesion = TurnTo(cohesion);

            separation /= nBoids;

            if (separation.magnitude > 0)
            {
                separation = (separation.normalized * speed) - velocity;
                separation = Vector2.ClampMagnitude(separation, maxForce);
            }

            return align*alignWeight + cohesion*cohesionWeight + separation*separationWeight;
        }
        return Vector2.zero;
    }

    private Vector2 TurnTo(Vector2 target)
    {
        Vector2 desired = (target - position).normalized * speed;
        Vector2 res = desired - velocity;
        res = Vector2.ClampMagnitude(res, maxForce);
        return res;
    }


    Vector2 RotatePoint(Vector2 point, Vector2 center, float angle)
    {
        float s = Mathf.Sin(angle);
        float c = Mathf.Cos(angle);
        point -= center;
        return new Vector2(point.x * c - point.y * s, point.x * s + point.y * c) +center;
    }

    Vector2 AvoidObstacles()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, velocity, vision, obstacleMask);
        if (hit.collider == null) return Vector2.zero;
        
        float hitDistance = Vector2.Distance((Vector2)transform.position, hit.point);

        Vector2 forwardPoint = (Vector2)transform.position + velocity;
        for(int i=1; i<10; i++)
        {
            Vector2 rayDir = RotatePoint(forwardPoint, transform.position, -2 * (float)Math.PI * i/10) - (Vector2)transform.position;
            hit = Physics2D.Raycast(transform.position, rayDir, vision, obstacleMask);
            if (hit.collider == null) return rayDir * 1/(hitDistance*hitDistance);
        }

        return Vector2.zero;
    }
    
    Vector2 FollowLider()
    {
        foreach(Boid b in nearbyBoids)
        {
            if (b.isLeader)
            {
                float d = Vector2.Distance(position, b.position);
                Vector2 leaderTail = b.position - (b.velocity.normalized * distanceToLeader);

                return Vector2.ClampMagnitude(leaderTail - position, maxForce);
            }
        }

        return Vector2.zero;
    }

    private void Edges(){
        if (transform.position.x > screenWidth/2)
        {
            transform.position = new Vector2(-screenWidth / 2 + 1, transform.position.y);
            position = transform.position;
        }
        else if (transform.position.x < -screenWidth / 2)
        {
            transform.position = new Vector2(screenWidth/2-1, transform.position.y);
            position = transform.position;
        }
        if (transform.position.y > screenHeight/2)
        {
            transform.position = new Vector2(transform.position.x, -screenHeight / 2+1);
            position = transform.position;
        }
        else if (transform.position.y < -screenHeight / 2)
        {
            transform.position = new Vector2(transform.position.x, screenHeight/2-1);
            position = transform.position;
        }
    }

    private void FixedUpdate()
    {
        Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, vision, boidMask);
        nBoids = hits.Length;
        nearbyBoids = new Boid[hits.Length];
        for(int i=0; i<hits.Length; i++)
            nearbyBoids[i] = hits[i].GetComponent<Boid>();

        position = transform.position;

        if (!isLeader)
        {
            acceleration = ComputeAcceleration();
            acceleration += FollowLider();
        }

        acceleration += AvoidObstacles();

        velocity += acceleration;
        velocity = Vector2.ClampMagnitude(velocity, speed);
        position += velocity;
        acceleration = Vector2.zero;
        transform.position = position;
        transform.right = Vector2.Lerp(transform.right,velocity, Time.deltaTime*damping);

        Edges();
    }
}