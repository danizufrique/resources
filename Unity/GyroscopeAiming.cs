﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroscopeAiming : MonoBehaviour
{
    [SerializeField] Vector2 speed = new Vector2(.65f, .3f);
    Rigidbody2D rb;

    void Start()
    {
        Input.gyro.enabled = true;
        rb = GetComponent<Rigidbody2D>();

        transform.position = Vector2.zero;
    }

    void Update()
    {        
        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Collider2D hitCollider = Physics2D.OverlapPoint(transform.position);
            if (hitCollider)
            {
                Target target = hitCollider.gameObject.GetComponent<Target>();

                if (target != null)
                {
                    target.Hit();
                }
            }
        }

        #region Clamp Position
        Vector2 clampPos;
        clampPos.x = Mathf.Clamp(transform.position.x, Controller.topLeftCorner().x, Controller.topRightCorner().x);
        clampPos.y = Mathf.Clamp(transform.position.y, Controller.botRightCorner().y, Controller.topLeftCorner().y);
        transform.position = clampPos;
        #endregion
    }
    void FixedUpdate()
    {
        
        Vector3 movement = new Vector3(Input.gyro.rotationRate.y * speed.x, -Input.gyro.rotationRate.x * speed.y);
        transform.position += movement;
    }

    bool CheckInsideView()
    {
        bool result = true;
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);

        if (pos.x < 0.0) result = false;
        else if (1.0 < pos.x) result = false;
        else if (pos.y < 0.0) result = false;
        else if (1.0 < pos.y) result = false;

        return result;
    }
}
