using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LighBounce : MonoBehaviour
{
    public int maxReflections = 3;
    private int currentReflections;
    private List<Vector3> bouncePoints;
    private float currentDistance;
    public float maxDistance;

    public Transform spawnPoint;
    public LineRenderer line;
    public LayerMask bounceLayer;
    

    private void Start()
    {
        bouncePoints = new List<Vector3>();
    }


    private void Update()
    {
        currentReflections = 0;
        currentDistance = 0;
        bouncePoints.Clear();
        bouncePoints.Add(transform.position);

        Reflect(transform.position, (spawnPoint.position-transform.position).normalized, maxDistance);
        line.positionCount = bouncePoints.Count;
        line.SetPositions(bouncePoints.ToArray());
    }

    void Reflect(Vector2 origin, Vector2 direction, float distance)
    {
        if (currentReflections >= maxReflections) return;

        RaycastHit2D hitData = Physics2D.Raycast(origin, direction.normalized, distance, bounceLayer);
        if (hitData)
        {
            currentReflections++;
            
            if(currentDistance+hitData.distance >= maxDistance)
            {
                bouncePoints.Add(origin + direction * Mathf.Clamp(maxDistance - currentDistance, 0, maxDistance));
                return;
            }

            currentDistance += hitData.distance;
            bouncePoints.Add(hitData.point);
            
            Vector2 newDir = Vector2.Reflect(direction, hitData.normal);
            Vector2 newOrigin = hitData.point + newDir * 0.01f;

            Reflect(newOrigin, newDir, maxDistance);
        }
        else
        {
            bouncePoints.Add(origin + direction*Mathf.Clamp(maxDistance-currentDistance, 0, maxDistance) );
        }
    }

    
}
