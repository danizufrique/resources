﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PivotCamera : MonoBehaviour
{
    public Transform target;

    
    public float speed;
    public float minDistance = 10;
    public float maxDistance = 3;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);

        if (Input.GetMouseButton(1))
        {
            float v = Input.GetAxis("Vertical");
            float d = Vector3.Distance(transform.position, target.position);

            if (v > 0)
            {
                if(d>maxDistance)
                    transform.position = Vector3.MoveTowards(transform.position, target.position, Input.GetAxis("Vertical"));
            }
            else if (v < 0)
            {
                if (d < minDistance)
                    transform.position = Vector3.MoveTowards(transform.position, target.position, Input.GetAxis("Vertical"));
            }
            
        }
        else
            transform.RotateAround(target.position, transform.right, Input.GetAxis("Vertical") * speed);
        
        transform.RotateAround(target.position, transform.up, -Input.GetAxis("Horizontal") * speed);


    }
}
