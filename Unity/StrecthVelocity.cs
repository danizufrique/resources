using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StretchVelocity : MonoBehaviour
{
    public Transform spriteObject;
    public float stretch = 0.1f;

    public Rigidbody2D rb;
    private Transform parent;
    private Vector3 initScale;

    private void Start()
    {
        parent = new GameObject(string.Format("_squash_{0}", name)).transform;
        initScale = spriteObject.transform.localScale;
    }

    private void FixedUpdate()
    {
        spriteObject.parent = transform;
        spriteObject.localPosition = Vector3.zero;
        spriteObject.localScale = initScale;
        spriteObject.localRotation = Quaternion.identity;

        parent.localScale = Vector3.one;
        parent.position = transform.position;

        Vector3 velocity =  (Vector3)rb.velocity;
        if (velocity.sqrMagnitude > .01f)
            parent.rotation = Quaternion.FromToRotation(Vector3.right, velocity);
        

        float scaleX = 1.0f + (velocity.magnitude * stretch/100);
        float scaleY = 1.0f / scaleX;
        spriteObject.parent = parent;
        parent.localScale = new Vector3(scaleX, scaleY, 1.0f);
    }
}
